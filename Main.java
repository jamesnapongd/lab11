import java.io.IOException;
import java.util.Scanner;


public class Main {
	private static final int port = 5001;
	public static void main(String[] args){
		Client client = new Client("158.108.181.241",port);
		try{
			client.openConnection();			
		}catch(IOException e){
			System.out.println("Couldn't start server:");
			System.out.println();
		}
		Scanner scanner = new Scanner(System.in);
		while(client.isConnected()){
			String msg = scanner.nextLine();
			try{
				client.sendToServer(msg);
				if(msg.equalsIgnoreCase("quit")){
					client.closeConnection();
					System.out.print("disconnect");
				}
			}
				catch(IOException e){
					e.getMessage();
				}
			scanner.close();
			}
		
		}
	}
